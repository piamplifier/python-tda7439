Python-TDA7439
--------------
This Python library is inteded to control the TDA7439 over I2C.
It is based on the Python smbus2(https://pypi.org/project/smbus2/) module.

The TDA7439 is a tone control IC from ST Microelectronics.
It can be used to digitally control volume, bass gain, treble gain,...
Also 4 audio inputs are present which can be switched digitally also.

Usage
-----
This module is very basic and simple.
It is intended to easily write the registers of the IC and nothing more.
Not complex or advanced methods are included in this module.

Example code:
```
import tda7439
# Create the object and select input1 by default
tone_ic = tda7439.tda7439('IN1')
# Volume is now -10 dB
tone_ic.volume = 10
# Get the current treble gain
print(tone_ic.treble_gain)
```

After intializing the tda7439 object, the registers in the IC are written
with default values. Below the different default values are listed:
  - Selected Input: IN2
  - Input gain: 30 dB
  - Volume: Muted
  - Bass gain: 0 dB
  - Mid gain: 0 dB
  - Treble gain: 0 dB
  - Left Speaker Attenuation: 0 dB
  - Right Speaker Attenuation: 0 dB

Platforms
---------
The module was written and tested on the Raspberry Pi.
