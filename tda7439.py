""" Python-TDA7439
    Copyright (C) 2021  Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Module to control the TDA7439 tone control IC.

    The TDA7439 is a tone control IC from
    ST Microelectronics which allows to digitally
    control volume, bass gain,... over I2C.
    It also supports input selection over 4
    different inputs.
    This makes it an easy to use highly integrated
    solution. The only downside is that the IC does
    not support register read out. It is only possible
    to write to the internal registers.

    This library is based on the Python smbus module
    and was tested on a Raspberry Pi.

    Example:
        import tda7439
        # Create a new object and set the input to IN1.
        tone_ic = tda7439.tda7430(input = 'IN1')
        # Set the volume to 10
        tone_ic.volume = 10
        # Get the current bass_gain
        print(tone_ic.bass_gain)
"""

import smbus2
import threading

# Gloabl constants
VOLUME_MUTE = 0x3F
SPEAKER_MUTE = 0xE8


class tda7439():
    """ Class to control the TDA7439 over I2C.
    """
    # The device address is fixed
    address = 0x44
    # Register map
    register = {'Input selector': 0x0, 'Input gain': 0x1,
                'Volume': 0x2, 'Bass gain': 0x3, 'Mid-range gain': 0x4,
                'Treble gain': 0x5, 'Speaker attenuation, R': 0x6,
                'Speaker attenuation, L': 0x7}
    # Possible inputs
    input_sel = {'IN1': 0x3, 'IN2': 0x2, 'IN3': 0x1, 'IN4': 0x0}

    def __init__(self, input='IN2', inp_gain=30, volume=VOLUME_MUTE,
                 bass_gain=0, mid_gain=0, treble_gain=0,
                 lspeaker_gain=0, rspeaker_gain=0):
        # New I2C interface using the smbus module.
        self.i2c_intf = smbus2.SMBus(1)
        # Set default values.
        self.lock = threading.Lock()
        self.input = input
        self.input_gain = inp_gain
        self.volume = volume
        self.bass_gain = bass_gain
        self.mid_gain = mid_gain
        self.treble_gain = treble_gain
        self.speaker_l_gain = rspeaker_gain
        self.speaker_r_gain = lspeaker_gain

    @property
    def input(self):
        """ Get the current input.
        """
        return self._selected_input

    @input.setter
    def input(self, inp):
        """ Select the current input.
        """
        self.i2c_intf.write_byte_data(
            self.address,
            self.register['Input selector'],
            self.input_sel[inp])
        self._selected_input = inp

    @property
    def input_gain(self):
        """ Get the current input amplifier gain.

            Gain is set in 16 steps of 2dB. 0 = 0dB, 15 = 30dB
        """
        return self._input_gain

    @input_gain.setter
    def input_gain(self, gain):
        """ Set the input amplifier gain.

            gain: The gain is set in 16 steps of 2dB. With 0 = 0dB
                  and 15 = 30dB.
        """
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Input gain'],
                                      gain)
        self.lock.release()
        self._input_gain = gain

    @property
    def volume(self):
        """ Get the current volume attenuation.

            0 = 0db, 40 = -40dB, 56 = Muted.
        """
        return self._volume

    @volume.setter
    def volume(self, attenuation):
        """ Set the volume attenuation.

            attenuation: The attenuation is set in 41 steps
                         of -1dB. 0 = 0dB, 40 = -40dB.
        """
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Volume'],
                                      attenuation)
        self.lock.release()
        self._volume = attenuation

    @property
    def bass_gain(self):
        """ Get the bass gain.

            Gain in dB.
        """
        return self._bass_gain

    @bass_gain.setter
    def bass_gain(self, gain):
        """ Set the bass gain.

            gain: A value in dB between 14 and -14dB in 2dB steps.
        """
        reg_gain = int(15 - (gain/2))
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Bass gain'],
                                      reg_gain)
        self.lock.release()
        self._bass_gain = gain

    @property
    def mid_gain(self):
        """ Get the mid-range gain.

            Gain in dB.
        """
        return self._mid_gain

    @mid_gain.setter
    def mid_gain(self, gain):
        """ Set the mid-range gain.

            gain: A value in dB between 14 and -14dB in 2dB steps.
        """
        reg_gain = int(15 - (gain/2))
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Mid-range gain'],
                                      reg_gain)
        self.lock.release()
        self._mid_gain = gain

    @property
    def treble_gain(self):
        """ Get the treble gain.

            Gain in dB.
        """
        return self._treble_gain

    @treble_gain.setter
    def treble_gain(self, gain):
        """ Set the treble gain.

            gain: A value in dB between 14 and -14dB in 2dB steps.
        """
        reg_gain = int(15 - (gain/2))
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Treble gain'],
                                      reg_gain)
        self.lock.release()
        self._treble_gain = gain

    @property
    def speaker_l_gain(self):
        """ Get the left speaker attenuation.

            Attenuation in dB.
        """
        return self._left_att

    @speaker_l_gain.setter
    def speaker_l_gain(self, attenuation):
        """ Set the left speaker attenuation.

            attenuation: Value between 0dB and 72dB in steps of 1dB.
                                Use SPEAKER_MUTE to mute the speaker.
        """
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Speaker attenuation, L'],
                                      attenuation)
        self.lock.release()
        self._left_att = attenuation

    @property
    def speaker_r_gain(self):
        """ Get the right speaker attenuation.

            Attenuation in dB.
        """
        return self._right_att

    @speaker_r_gain.setter
    def speaker_r_gain(self, attenuation):
        """ Set the right speaker attenuation.

            :param: attenuation Value between 0dB and 72dB in steps of 1dB.
                                Use SPEAKER_MUTE to mute the speaker.
        """
        self.lock.acquire()
        self.i2c_intf.write_byte_data(self.address,
                                      self.register['Speaker attenuation, R'],
                                      attenuation)
        self.lock.release()
        self._right_att = attenuation
